/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState} from 'react';
import {
  Image,
  ImageSourcePropType,
  LayoutChangeEvent,
  LayoutRectangle,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import axios from 'axios';
// @ts-ignore
import addPhotoPlaceholder from './images/add-photo-placeholder.png';

const photoURLs = [
  'https://thumbor.forbes.com/thumbor/fit-in/416x416/filters%3Aformat%28jpg%29/https%3A%2F%2Fspecials-images.forbesimg.com%2Fimageserve%2F5f4ebe0c87612dab4f12a597%2F0x0.jpg%3Fbackground%3D000000%26cropX1%3D292%26cropX2%3D3684%26cropY1%3D592%26cropY2%3D3987',
  'https://pbs.twimg.com/profile_images/988775660163252226/XpgonN0X.jpg',
  'https://upload.wikimedia.org/wikipedia/commons/a/a0/Bill_Gates_2018.jpg',
  'https://tendercapital.com/wp-content/uploads/2018/07/YAM112003_HungryMinds_Agosto_Articolo6_Foto_DEF-1024x684.jpg',
  'https://2.bp.blogspot.com/-1FWYMMzEtCY/TcP7yBsYhiI/AAAAAAAAClM/Md9mPa8xb44/s1600/Bill-Gates-2.jpg',
  'https://img.timeinc.net/time/photoessays/2010/top10_dropouts/bill_gates.jpg',
  'https://i1.wp.com/eveningmailgh.com/wp-content/uploads/2019/07/416x416.jpg?fit=416%2C416&ssl=1',
  'https://abesipr.files.wordpress.com/2012/01/bill-gates-profile.jpg',
  'https://media.wired.com/photos/5f2c4e757055a2aa4e463797/1:1/w_650,h_650,c_limit/Science_BillGates_1174744356.jpg',
];

const baseURL = 'http://localhost:3000';

const addPhoto = async (
  url: string,
  position: number,
): Promise<APIAddPhotoResponse> =>
  (
    await axios.post(`${baseURL}/member/1/photos`, {
      url: url,
      position: position,
    })
  ).data;

const deletePhoto = async (photoID: string) =>
  await axios.delete(`${baseURL}/photos/${photoID}`);

const getPhotos = async (): Promise<APIPhotoBase[]> => {
  return (await axios.get(`${baseURL}/member/1/photos`)).data;
};

interface APIPhotoBase {
  id: string;
  url: string;
  width: number;
  height: number;
  position: number;
  centerX: number;
  centerY: number;
}

interface APIAddPhotoResponse {
  url: string;
  position: string;
  memberId: string;
  id: string;
}

interface PhotoMetadata {
  isUploaded: boolean;
  source: ImageSourcePropType;
  position: number;
  id: string;
}

interface PhotoCellProps {
  photoDimentions: LayoutRectangle;
  metadata: PhotoMetadata;
}

const PhotoCell = (props: PhotoCellProps) => {
  const {photoDimentions, metadata} = props;
  const [image, setImage] = useState<PhotoMetadata>(metadata);

  const handleAddImagePress = async () => {
    if (!image.isUploaded) {
      const photo = await addPhoto(
        photoURLs[metadata.position],
        metadata.position,
      );

      setImage((oldState) => {
        return {
          position: oldState.position,
          isUploaded: true,
          source: {uri: photoURLs[metadata.position]},
          id: photo.id,
        };
      });

      return;
    }

    await deletePhoto(image.id);

    setImage((oldState) => {
      return {
        position: oldState.position,
        isUploaded: false,
        source: addPhotoPlaceholder,
        id: '',
      };
    });
  };

  return (
    <View style={styles.cell}>
      <TouchableOpacity onPress={handleAddImagePress}>
        <Image
          source={image.source}
          style={{
            height: photoDimentions.height / 3,
            width: photoDimentions.width / 3,
            resizeMode: 'contain',
          }}
        />
      </TouchableOpacity>
    </View>
  );
};

const App = () => {
  const [photoDimentions, setPhotoDimentions] = useState<LayoutRectangle>({
    width: 0,
    height: 0,
    x: 0,
    y: 0,
  } as LayoutRectangle);

  const photosMetadata: PhotoMetadata[] = [];

  for (let i = 0; i < 9; i++) {
    photosMetadata.push({
      isUploaded: false,
      position: i,
      source: addPhotoPlaceholder,
      id: '',
    });
  }

  useEffect(() => {
    const doAsync = async () => {
      const photos = await getPhotos();
      photos.map((photo) => {
        photosMetadata[photo.position].id = photo.id;
        photosMetadata[photo.position].isUploaded = true;
        photosMetadata[photo.position].position = photo.position;
        photosMetadata[photo.position].source = {
          uri: photo.url,
        };
      });
    };

    doAsync();
  }, []);

  const handleOnRootLayout = (e: LayoutChangeEvent) => {
    setPhotoDimentions(e.nativeEvent.layout);
  };

  return (
    <SafeAreaView style={styles.root} onLayout={handleOnRootLayout}>
      <View style={styles.row}>
        <PhotoCell
          photoDimentions={photoDimentions}
          metadata={photosMetadata[0]}
        />
        <PhotoCell
          photoDimentions={photoDimentions}
          metadata={photosMetadata[1]}
        />
        <PhotoCell
          photoDimentions={photoDimentions}
          metadata={photosMetadata[2]}
        />
      </View>
      <View style={styles.row}>
        <PhotoCell
          photoDimentions={photoDimentions}
          metadata={photosMetadata[3]}
        />
        <PhotoCell
          photoDimentions={photoDimentions}
          metadata={photosMetadata[4]}
        />
        <PhotoCell
          photoDimentions={photoDimentions}
          metadata={photosMetadata[5]}
        />
      </View>
      <View style={styles.row}>
        <PhotoCell
          photoDimentions={photoDimentions}
          metadata={photosMetadata[6]}
        />
        <PhotoCell
          photoDimentions={photoDimentions}
          metadata={photosMetadata[7]}
        />
        <PhotoCell
          photoDimentions={photoDimentions}
          metadata={photosMetadata[8]}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    marginLeft: 15,
    marginRight: 10,
  },
  row: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
  },
  cell: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default App;
